﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Coscine.SemanticSearch.Util.QueryObjects
{
    //Generated with JSON -> C#
    public partial class ElasticQuery
    {
        [JsonProperty("size", NullValueHandling = NullValueHandling.Ignore)]
        public long? Size { get; set; }

        [JsonProperty("from", NullValueHandling = NullValueHandling.Ignore)]
        public long? From { get; set; }

        [JsonProperty("sort", NullValueHandling = NullValueHandling.Ignore)]
        public List<Sort>? Sort { get; set; }

        [JsonProperty("_source", NullValueHandling = NullValueHandling.Ignore)]
        public Source Source { get; set; }

        [JsonProperty("query")]
        public Query Query { get; set; }
    }

    public partial class Query
    {
        [JsonProperty("bool")]
        public QueryBool Bool { get; set; }
    }

    public partial class QueryBool
    {
        [JsonProperty("must")]
        public List<Must> Must { get; set; }

        [JsonProperty("filter")]
        public Filter Filter { get; set; }
    }

    public partial class Filter
    {
        [JsonProperty("bool")]
        public FilterBool Bool { get; set; }
    }

    public partial class FilterBool
    {
        [JsonProperty("should")]
        public List<Should> Should { get; set; }
    }

    public partial class Should
    {
        [JsonProperty("terms", NullValueHandling = NullValueHandling.Ignore)]
        public Terms Terms { get; set; }

        [JsonProperty("term", NullValueHandling = NullValueHandling.Ignore)]
        public ShouldTerm Term { get; set; }
    }

    public partial class ShouldTerm
    {
        [JsonProperty("isPublic")]
        public bool IsPublic { get; set; }
    }

    public partial class Terms
    {
        [JsonProperty("belongsToProject")]
        public List<string> BelongsToProject { get; set; }
    }

    public partial class Must
    {
        [JsonProperty("simple_query_string", NullValueHandling = NullValueHandling.Ignore)]
        public SimpleQueryString SimpleQueryString { get; set; }

        [JsonProperty("query_string", NullValueHandling = NullValueHandling.Ignore)]
        public QueryString QueryString { get; set; }

        [JsonProperty("term", NullValueHandling = NullValueHandling.Ignore)]
        public MustTerm Term { get; set; }
    }

    public partial class SimpleQueryString
    {
        [JsonProperty("query")]
        public string Query { get; set; }
    }

    public partial class QueryString
    {
        [JsonProperty("query")]
        public string Query { get; set; }
    }

    public partial class MustTerm
    {
        [JsonProperty("structureType.keyword")]
        public string StructureType { get; set; }
    }

    public partial class Source
    {
    }

    public partial class Sort
    {
        [JsonProperty("date_created", NullValueHandling = NullValueHandling.Ignore)]
        public DateCreated DateCreated { get; set; }

        [JsonProperty("_script", NullValueHandling = NullValueHandling.Ignore)]
        public Script Name { get; set; }

        [JsonProperty("_score", NullValueHandling = NullValueHandling.Ignore)]
        public SortElement Score { get; set; }
    }

    public partial class SortElement
    {
        [JsonProperty("order")]
        public string Order { get; set; } = "asc";
    }

    public partial class Script
    {
        [JsonProperty("type")]
        public string Type { get; set; } = "string";

        [JsonProperty("script")]
        public ScriptClass ScriptScript { get; set; } = new ScriptClass();

        [JsonProperty("order")]
        public string Order { get; set; } = "asc";
    }

    public partial class ScriptClass
    {
        [JsonProperty("lang")]
        public string Lang { get; set; } = "painless";

        [JsonProperty("source")]
        public string Source { get; set; } = "if (doc['fileName.keyword'].size() != 0) { doc['fileName.keyword'].value.toLowerCase()} else { doc['title.keyword'].value.toLowerCase()} ";
    }

    public partial class DateCreated
    {
        [JsonProperty("order")]
        public string Order { get; set; } = "asc";

        [JsonProperty("format")]
        public string Format { get; set; } = "yyyy-MM-dd";
    }
}