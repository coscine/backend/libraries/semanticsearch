﻿using System;

namespace Coscine.SemanticSearch.Util
{
    /// <summary>
    /// Exception which is thrown if a triple of a metadata graph could not be indexed.
    /// </summary>
    [Serializable]
    public class NotIndexableException : Exception
    {
        public NotIndexableException(string reason) : base() => Reason = reason;

        public string Reason { get; }
    }
}