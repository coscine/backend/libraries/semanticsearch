﻿using Coscine.SemanticSearch.Clients;
using Coscine.SemanticSearch.Core;
using Coscine.SemanticSearch.TripleCreator;
using Coscine.SemanticSearch.Util;
using Coscine.SemanticSearch.Util.QueryObjects;
using Newtonsoft.Json.Linq;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Coscine.SemanticSearch
{
    /// <summary>
    /// Contains the main logic to use a document-based search engine for RDF-based metadata records.
    /// </summary>
    public class RdfSearchMapper
    {
        // default search parameter
        private const int SEARCH_SIZE = 50;

        private const int SEARCH_FROM = 0;
        private const bool SEARCH_ADVANCED = false;

        private readonly ISearchClient _searchClient;
        private readonly RdfClient _rdfClient;

        private ElasticsearchIndexMapper _indexMapper;
        private int _version;

        private static readonly ILogger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Creates an instance of the class <c>RdfSearchMapper</c>.
        /// </summary>
        /// <param name="connector">Instance of an <c>IRdfConnector</c>.</param>
        /// <param name="searchClient">Instance of an <c>ISearchClient</c>.</param>
        /// <param name="languageList">Abbreviated string representation of the language used.</param>
        public RdfSearchMapper(IRdfConnector connector, ISearchClient searchClient, List<string> languageList)
        {
            _rdfClient = new RdfClient(connector, languageList);
            _searchClient = searchClient;

            _version = _rdfClient.GetCurrentIndexVersion();
            _searchClient.ChangeIndex(GetCurrentIndex());
        }

        /************** SEARCH ************/

        /// <summary>
        /// Executes a search query.
        /// </summary>
        /// <param name="query">The search query of the user.</param>
        /// <param name="user">The user who searches.</param>
        /// <param name="advanced">lag to specify simple or advanced search syntax.</param>
        /// <param name="size">Number of results.</param>
        /// <param name="from">Position from which the results should be returned.</param>
        /// <param name="sorting">Sorting of the results <see>
        /// (see https://www.elastic.co/guide/en/elasticsearch/reference/current/sort-search-results.html). </see></param>
        /// <param name="categoryFilter"></param>
        /// <returns>A task that represents the asynchronous search operation.</returns>
        public async Task<IDictionary<string, JObject>> SearchAsync(string query, string? user = null, bool advanced = SEARCH_ADVANCED, int size = SEARCH_SIZE, int from = SEARCH_FROM, Sort? sorting = null, CategoryFilter categoryFilter = CategoryFilter.None)
        {
            if (sorting is null)
            {
                sorting = new Sort
                {
                    Score = new SortElement()
                };
            }

            IEnumerable<string>? projects;
            if (string.IsNullOrWhiteSpace(user))
            {
                projects = null;
            }
            else
            {
                // projects are =0 or >0
                projects = _rdfClient.GetProjectsOfUser(user);
            }

            return await _searchClient.SearchAsync(query,
                projects,
                advanced,
                size,
                from,
                new List<Sort>
                {
                    sorting
                },
                categoryFilter
                );
        }

        /************** Count ************/

        public async Task<int> CountAsync(string query, string? user = null, bool advanced = SEARCH_ADVANCED, CategoryFilter categoryFilter = CategoryFilter.None)
        {
            IEnumerable<string>? projects;
            if (string.IsNullOrWhiteSpace(user))
            {
                projects = null;
            }
            else
            {
                // projects are =0 or >0
                projects = _rdfClient.GetProjectsOfUser(user);
            }
            return await _searchClient.CountAsync(query, projects, advanced, categoryFilter);
        }

        /************** ADD or UPDATE ************/

        /// <summary>
        /// Adds or updates a single document.
        /// </summary>
        /// <param name="graphName">ID of metadata graph.</param>
        /// <returns>A task that represents the asynchronous save operation.</returns>
        public async Task AddDocumentAsync(string graphName)
        {
            _indexMapper = new ElasticsearchIndexMapper(_rdfClient, await _searchClient.GetMappingAsync());
            var documents = CreateDocument(graphName);
            await _searchClient.AddDocumentAsync(graphName, documents);
        }

        /************** DELETE ************/

        /// <summary>
        /// Deletes a single document.
        /// </summary>
        /// <param name="graphName">ID of metadata graph.</param>
        /// <returns>A task that represents the asynchronous delete operation.</returns>
        public async Task DeleteDocumentAsync(string graphName)
        {
            _indexMapper = new ElasticsearchIndexMapper(_rdfClient, await _searchClient.GetMappingAsync());
            var applicationProfileId = _rdfClient.GetApplicationProfileOfMetadata(graphName);
            // Null should mean that no document exists to the graph
            if (applicationProfileId == null)
            {
                // Graph may only be deleted from the RDF database after the mapping has been deleted in the search engine!
                _rdfClient.MarkGraphAsDeleted(graphName);
            }
            else
            {
                // Graph may only be deleted from the RDF database after the mapping has been deleted in the search engine!
                _rdfClient.MarkGraphAsDeleted(graphName);

                await _searchClient.DeleteDocumentAsync(graphName, new Dictionary<string, Document>());
            }
        }

        /************** INITIAL INDEX ************/

        /// <summary>
        /// Creates a first index with indexes all existing metadata graphs.
        /// </summary>
        /// <returns>A task that represents the asynchronous create operation.</returns>
        public async Task CreateIndexAsync()
        {
            var structuredItems = RetrieveStructuredItems();

            _indexMapper = new ElasticsearchIndexMapper(_rdfClient);
            var content = _indexMapper.CreateIndex(ElasticsearchIndexMapper.DEFAULT_ALIAS_NAME);

            await _searchClient.CreateIndexAsync(content, GetCurrentIndex());

            await PerformIndexing(structuredItems);
        }

        /************** RE-INDEX ************/

        /// <summary>
        /// Creates a new index (and mapping) and reindexes all existing metadata graphs.
        /// </summary>
        /// <remarks>This operation is necessary if an application profile changes or a new is added.</remarks>
        /// <returns>A task that represents the asynchronous create operation.</returns>
        public async Task ReIndexAsync()
        {
            var structuredItems = RetrieveStructuredItems();

            // create new index
            _indexMapper = new ElasticsearchIndexMapper(_rdfClient);
            var content = _indexMapper.CreateIndex();
            await _searchClient.CreateIndexAsync(content, ElasticsearchIndexMapper.DEFAULT_ALIAS_NAME + "_" + (_version + 1));

            var oldIndex = GetCurrentIndex();
            _version++; // increase version
            _rdfClient.SetCurrentIndexVersion(_version);
            var newIndex = GetCurrentIndex();
            _searchClient.ChangeIndex(newIndex);

            await PerformIndexing(structuredItems);

            if (await _searchClient.ContainsIndexAsync(oldIndex))
            {
                // change aliases
                await _searchClient.SwitchAliasAsync(from: oldIndex, to: newIndex);

                // delete old index
                await _searchClient.DeleteIndexAsync(oldIndex);
            }
            else
            {
                // change aliases
                await _searchClient.SetAliasAsync(to: newIndex);
            }
        }

        /// <summary>
        /// The logic for performing the indexing step
        /// </summary>
        /// <returns></returns>
        private async Task PerformIndexing(List<(IEnumerable<string>, DefaultTripleCreator)> structuredItems)
        {
            _indexMapper.ReplaceMapping(await _searchClient.GetMappingAsync());

            var totalCount = structuredItems.Sum((list) => list.Item1.Count());

            // index all existing data
            var index = 0;
            foreach (var listOfIds in structuredItems)
            {
                var documents = new List<Document>();
                foreach (var graph in listOfIds.Item1)
                {
                    index++;
                    logger.Info("Working on document for entry number {index}/{totalCount}", index, totalCount);
                    try
                    {
                        var doc = CreateDocument(graph, listOfIds.Item2)[graph];
                        documents.Add(doc);
                    }
                    catch (Exception e)
                    {
                        logger.Info("Error while creating a document for graphname {graphname} with the error: {errorMessage}; {innerException}; {stackTrace}",
                            graph, e.Message, e.InnerException, e.StackTrace);
                    }
                }

                // Need to split in batches of 1000 elements when posting
                const int batchSize = 1000;
                for (int i = 0; i < documents.Count; i += batchSize)
                {
                    var batch = documents.Skip(i).Take(batchSize);
                    await _searchClient.AddDocumentsAsync(batch);
                    logger.Info("Posted {i}/{count}", i + batch.Count(), documents.Count);
                }
            }
        }

        /// <summary>
        /// Retrieves all structured items
        /// </summary>
        /// <returns></returns>
        private List<(IEnumerable<string>, DefaultTripleCreator)> RetrieveStructuredItems()
        {
            var publicResources = new List<string>();
            var resourceToProject = new Dictionary<string, string>();
            var resourceToApplicationProfile = new Dictionary<string, string>();
            var projectSlugs = new Dictionary<string, string>();

            var structuredItems = new List<(IEnumerable<string>, DefaultTripleCreator)>();
            structuredItems.AddRange(_rdfClient.GetAllProjects()
                .Select((entry) => (entry, (DefaultTripleCreator)new ProjectTripleCreator(projectSlugs))));
            structuredItems.AddRange(_rdfClient.GetAllResources()
                .Select((entry) => (entry, (DefaultTripleCreator)new ResourceTripleCreator(publicResources, resourceToProject, resourceToApplicationProfile))));
            structuredItems.AddRange(_rdfClient.GetAllMetadataIds()
                .Select((entry) => (entry, (DefaultTripleCreator)new MetadataTripleCreator(publicResources, resourceToProject, resourceToApplicationProfile, projectSlugs))));
            return structuredItems;
        }

        /// <summary>
        /// Creates the mapping of a metadata graph.
        /// </summary>
        /// <param name="graphName">ID of the metadata graph.</param>
        /// <param name="changeOtherDocs">A dictionary containing the ID of metadata graphs (key)
        /// <param name="tripleCreator">An implementation of DefaultTripleCreator
        /// and corresponding JSON objects (value).</param>
        /// <returns></returns>
        private IDictionary<string, Document> CreateDocument(string graphName, DefaultTripleCreator? tripleCreator = null)
        {
            var applicationProfileId = _rdfClient.GetApplicationProfileOfMetadata(graphName);
            if (applicationProfileId is not null && !_indexMapper.ApplicationProfiles.ContainsKey(applicationProfileId))
            {
                _indexMapper.ApplicationProfiles.Add(applicationProfileId, new SpecificApplicationProfile(_rdfClient, applicationProfileId));
            }
            var profile = (applicationProfileId is not null) ? _indexMapper.ApplicationProfiles[applicationProfileId] : null;

            // create properties
            var graph = _rdfClient.GetGraph(graphName);
            var document = _rdfClient.CreateFields(graph.Triples, profile, _indexMapper);

            if (tripleCreator is not null)
            {
                var fields = tripleCreator.CreateFields(graph, _indexMapper);
                document.Merge(fields);
            }

            return new Dictionary<string, Document> { { graphName, document } };
        }

        private string GetCurrentIndex()
        {
            return ElasticsearchIndexMapper.DEFAULT_ALIAS_NAME + "_" + _version;
        }
    }
}
