using Coscine.SemanticSearch.Core;
using Coscine.SemanticSearch.Util;
using Coscine.SemanticSearch.Util.QueryObjects;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;
using Polly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Mime;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Coscine.SemanticSearch.Clients
{
    /// <summary>
    /// Implements necessary functions to use Elasticsearch as a search engine.
    /// </summary>
    /// <inheritdoc cref="ISearchClient"/>
    public class ElasticsearchSearchClient : ISearchClient
    {
        // API
        private const string ALIASES = "_aliases";

        private const string SEARCH = "_search";
        private const string MAPPING = "_mapping";
        private const string BULK = "_bulk";
        private const string COUNT = "_count";

        private static readonly HttpClient _httpClient = new();

        private readonly string baseUrl;

        private string _index;

        private static readonly ILogger logger = LogManager.GetCurrentClassLogger();

        public ElasticsearchSearchClient(string server = "localhost", string port = "9200")
        {
            baseUrl = $"http://{server}:{port}/";
            _httpClient.DefaultRequestHeaders.Add("Accept", "application/json");
        }

        public void ChangeIndex(string index)
        {
            _index = index;
        }

        public async Task<IDictionary<string, string>> GetMappingAsync()
        {
            var response = await _httpClient.GetAsync(baseUrl + _index + "/" + MAPPING);
            JObject jObject = JObject.Parse(await response.Content.ReadAsStringAsync());
            var fieldMappings = (JObject)jObject[_index]["mappings"]["properties"];

            IDictionary<string, string> mapping = new Dictionary<string, string>();
            foreach (var prop in fieldMappings)
            {
                mapping.Add(prop.Key, (string)prop.Value["type"]);
            }
            return mapping;
        }

        /// <summary>
        /// Queries the document ID for a metadata graph.
        /// </summary>
        /// <param name="graphName">ID of the metadata graph.</param>
        /// <returns>ID of the Elasticsearch document.</returns>
        private async Task<string> GetIdFromGraphNameAsync(string graphName)
        {
            var content = new JObject() {
                new JProperty("query", new JObject
                {
                    new JProperty("term", new JObject
                    {
                        new JProperty(ElasticsearchIndexMapper.LABEL_GRAPHNAME, new JObject
                        {
                            new JProperty("value", graphName)
                        })
                    })
                })
            };

            var response = await _httpClient.PostAsync(baseUrl + _index + "/" + SEARCH, CreateJsonContent(content));

            JObject jObject = JObject.Parse(await response.Content.ReadAsStringAsync());
            if ((int)jObject["hits"]["total"]["value"] == 1)
            {
                return (string)jObject["hits"]["hits"][0]["_id"];
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Handles response of a HTTP request.
        /// </summary>
        /// <param name="response">The response of a HTTP request.</param>
        private static void HandleResponse(HttpResponseMessage response)
        {
            if (!response.IsSuccessStatusCode)
            {
                // TODO
                logger.Info("{statusCode}, {content}, {requestMessage}", response.StatusCode, response.Content, response.RequestMessage);
                // Console.WriteLine(response.StatusCode);
                // Console.WriteLine(response.Content);
                // Console.WriteLine(response.RequestMessage);
            }
        }

        // create json content from JObject
        private static HttpContent CreateJsonContent(JObject content)
        {
            return new StringContent(content.ToString(), Encoding.UTF8, "application/json");
        }

        // create json content from string
        private static HttpContent CreateJsonContent(string content)
        {
            return new StringContent(content, Encoding.UTF8, "application/json");
        }

        public async Task CreateIndexAsync(JObject content, string index)
        {
            _ = await _httpClient.DeleteAsync(baseUrl + index); //
                                                                //
                                                                // HERE: Figure out if deletion is always necessary.
            var response = await _httpClient.PutAsync(baseUrl + index, CreateJsonContent(content));
            response.EnsureSuccessStatusCode();
        }

        public async Task AddDocumentsAsync(IEnumerable<Document> documents)
        {
            logger.Info("Adding Documents");
            // Console.WriteLine("Adding Documents");
            var contentList = new List<string>();
            foreach (var document in documents)
            {
                // create new document
                contentList.Add("{ \"index\" : {  } }");
                contentList.Add(document.ToString().Replace("\n", "").Replace("\r", ""));
            }

            var content = string.Join("\n", contentList) + "\n";
            var response = await WrapRequest(async () => await _httpClient.PostAsync(baseUrl + _index + "/" + BULK, CreateJsonContent(content)));
            HandleResponse(response);
            logger.Info("Added Documents");
            // Console.WriteLine("Added Documents");
        }

        /// <summary>
        /// Retry ES Requests since they sometimes just fail
        /// </summary>
        /// <typeparam name="W"></typeparam>
        /// <param name="function"></param>
        /// <returns>Response</returns>
        public static async Task<W> WrapRequest<W>(Func<Task<W>> function)
        {
            var policyWrapper = await Policy
                .Handle<Exception>()
                .WaitAndRetryAsync(5, _ => TimeSpan.FromSeconds(5))
                .ExecuteAndCaptureAsync(async () => await function.Invoke());

            return policyWrapper.Result;
        }

        public async Task<bool> ContainsIndexAsync(string index)
        {
            var response = await _httpClient.GetAsync(baseUrl + index);
            return response.IsSuccessStatusCode;
        }

        public async Task SetAliasAsync(string to)
        {
            var jObject = new JObject {
                new JProperty("actions", new JArray
                {
                    new JObject
                    {
                        new JProperty("add", new JObject
                        {
                            new JProperty("alias", ElasticsearchIndexMapper.DEFAULT_ALIAS_NAME),
                            new JProperty("index", to)
                        })
                    }
                })
            };

            await WrapRequest(async () => {
                var response = await _httpClient.PostAsync(baseUrl + ALIASES, CreateJsonContent(jObject));
                response.EnsureSuccessStatusCode();
                return response;
            });
        }

        public async Task SwitchAliasAsync(string from, string to)
        {
            var jObject = new JObject {
                new JProperty("actions", new JArray
                {
                    new JObject
                    {
                        new JProperty("remove", new JObject
                        {
                            new JProperty("alias", ElasticsearchIndexMapper.DEFAULT_ALIAS_NAME),
                            new JProperty("index", from)
                        })
                    },
                    new JObject
                    {
                        new JProperty("add", new JObject
                        {
                            new JProperty("alias", ElasticsearchIndexMapper.DEFAULT_ALIAS_NAME),
                            new JProperty("index", to)
                        })
                    }
                })
            };

            await WrapRequest(async () => {
                var response = await _httpClient.PostAsync(baseUrl + ALIASES, CreateJsonContent(jObject));
                response.EnsureSuccessStatusCode();
                return response;
            });
        }

        public async Task DeleteIndexAsync(string index)
        {
            var response = await _httpClient.DeleteAsync(baseUrl + index);
            HandleResponse(response);
        }

        public async Task AddDocumentAsync(string graphName, IDictionary<string, Document> documents)
        {
            var contentList = new List<string>();
            foreach (var document in documents)
            {
                if (string.Equals(document.Key, graphName))
                {
                    var id = await GetIdFromGraphNameAsync(document.Key);
                    // use id if this document already exists
                    var useId = string.IsNullOrEmpty(id) ? "" : $"\"_id\":\"{id}\"";

                    // create new document
                    contentList.Add($"{{ \"index\" : {{ {useId} }} }}");
                    contentList.Add(document.Value.ToString().Replace("\n", "").Replace("\r", ""));
                }
                else
                {
                    contentList = await AddOtherDocuments(contentList, document);
                }
            }
            await BulkRequestAsync(contentList);
        }

        /// <summary>
        /// Creates the content list for a bulk request with the given documents.
        /// </summary>
        /// <param name="contentList">A list containing the content for the bulk request.</param>
        /// <param name="document">IDs of metadata graphs (key) and their correcsponding content as JSON object (value).</param>
        /// <returns>The task result contains the created content list for the bulk request.</returns>
        private async Task<List<string>> AddOtherDocuments(List<string> contentList, KeyValuePair<string, Document> document)
        {
            var id = await GetIdFromGraphNameAsync(document.Key);
            // it could take some time while new document is indexed and ready for search
            while (string.IsNullOrEmpty(id))
            {
                logger.Info("Wait for elasticsearch _id of graph {documentKey}", document.Key);
                // Console.WriteLine($"Wait for elasticsearch _id of graph {document.Key}");
                Thread.Sleep(100);
                id = await GetIdFromGraphNameAsync(document.Key);
            }
            // update existing document
            contentList.Add($"{{ \"update\" : {{ \"_id\" : \"{id}\" }} }}");
            contentList.Add($"{{ \"doc\": {document.Value} }}".Replace("\n", "").Replace("\r", ""));
            return contentList;
        }

        public async Task DeleteDocumentAsync(string graphName, IDictionary<string, Document> documents)
        {
            var contentList = new List<string>
            {
                $"{{ \"delete\" : {{ \"_id\":\"{await GetIdFromGraphNameAsync(graphName)}\" }} }}"
            };
            foreach (var document in documents)
            {
                contentList = await AddOtherDocuments(contentList, document);
            }
            await BulkRequestAsync(contentList);
        }

        /// <summary>
        /// Executes a bulk request with the given list of content.
        /// </summary>
        /// <param name="contentList">An enumerator of content rows for the bulk request.</param>
        /// <returns>A task that represents the asynchronous save operation.</returns>
        private async Task BulkRequestAsync(IEnumerable<string> contentList)
        {
            var content = string.Join("\n", contentList) + "\n";
            // bulk request because constructed additional triples could change other documents..
            var response = await _httpClient.PostAsync(baseUrl + _index + "/" + BULK, CreateJsonContent(content));
            HandleResponse(response);
        }

        // search
        public async Task<IDictionary<string, JObject>> SearchAsync(string query, IEnumerable<string>? projects, bool advanced, int size, int from, IEnumerable<Sort>? sorting = null, CategoryFilter categoryFilter = CategoryFilter.None)
        {
            var searchQuery = new ElasticQuery
            {
                Size = size,
                From = from,
                Sort = sorting?.ToList(),
                Source = new Source(),
                Query = new Query
                {
                    Bool = new QueryBool
                    {
                        Must = new List<Must>(),
                        Filter = new Filter
                        {
                            Bool = new FilterBool
                            {
                                Should = new List<Should>
                                {
                                    new Should
                                    {
                                        Terms = new Terms
                                        {
                                            BelongsToProject = projects?.Any() == true ? projects.ToList() : new List<string>()
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            };

            // Add the category filter

            if (categoryFilter != CategoryFilter.None)
            {
                searchQuery.Query.Bool.Must.Add(new Must
                {
                    Term = new MustTerm
                    {
                        StructureType = categoryFilter.GetDescription()
                    }
                });
            }

            // Add search query for advanced or simple

            if (!advanced)
            {
                searchQuery.Query.Bool.Must.Add(new Must
                {
                    SimpleQueryString = new SimpleQueryString
                    {
                        Query = query
                    }
                });
            }
            else
            {
                searchQuery.Query.Bool.Must.Add(new Must
                {
                    QueryString = new QueryString
                    {
                        Query = query
                    }
                });
            }

            // Public elements are always included
            searchQuery.Query.Bool.Filter.Bool.Should.Add(new Should
            {
                Term = new ShouldTerm
                {
                    IsPublic = true
                }
            });
            return await Search(JsonConvert.SerializeObject(searchQuery));
        }

        public async Task<int> CountAsync(string query, IEnumerable<string>? projects, bool advanced, CategoryFilter categoryFilter = CategoryFilter.None)
        {
            // Initialize basic query
            var countQuery = new ElasticQuery
            {
                Query = new Query
                {
                    Bool = new QueryBool
                    {
                        Must = new List<Must>(),
                        Filter = new Filter
                        {
                            Bool = new FilterBool
                            {
                                Should = new List<Should>
                                {
                                    new Should
                                    {
                                        Terms = new Terms
                                        {
                                            BelongsToProject = projects?.Any() == true ? projects.ToList() : new List<string>()
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            };

            // Add the category filter

            if (categoryFilter != CategoryFilter.None)
            {
                countQuery.Query.Bool.Must.Add(new Must
                {
                    Term = new MustTerm
                    {
                        StructureType = categoryFilter.GetDescription()
                    }
                });
            }

            // Add search query for advanced or simple

            if (!advanced)
            {
                countQuery.Query.Bool.Must.Add(new Must
                {
                    SimpleQueryString = new SimpleQueryString
                    {
                        Query = query
                    }
                });
            }
            else
            {
                countQuery.Query.Bool.Must.Add(new Must
                {
                    QueryString = new QueryString
                    {
                        Query = query
                    }
                });
            }

            // Public elements are always included
            countQuery.Query.Bool.Filter.Bool.Should.Add(new Should
            {
                Term = new ShouldTerm
                {
                    IsPublic = true
                }
            });

            return await Count(JsonConvert.SerializeObject(countQuery));
        }

        /// <summary>
        /// Runs the search and handles the response.
        /// </summary>
        /// <param name="content">A JSON object containing the body of the search request.</param>
        /// <returns>The task result contains a dictionary containing the IDs of the found metadata graphs (key)
        /// and the corresponding ranking (value).</returns>
        private async Task<IDictionary<string, JObject>> Search(string content)
        {
            var response = await _httpClient.PostAsync(baseUrl + ElasticsearchIndexMapper.DEFAULT_ALIAS_NAME + "/" + SEARCH, CreateJsonContent(content));
            if (response.IsSuccessStatusCode)
            {
                JObject jObject = JObject.Parse(await response.Content.ReadAsStringAsync());
                return GetSearchResults(jObject);
            }
            else
            {
                // TODO: for malformed query
                // ["error"]["root_cause"]["reason"]
                // ["error"]["root_cause"]["type"]
                HandleResponse(response);
                return new Dictionary<string, JObject>();
            }
        }

        /// <summary>
        /// Runs the count and handles the response.
        /// </summary>
        /// <param name="content">A JSON object containing the body of the search request.</param>
        /// <returns>The task result is the amount of found results for the query.</returns>
        private async Task<int> Count(string content)
        {
            var request = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri($"{baseUrl}{ElasticsearchIndexMapper.DEFAULT_ALIAS_NAME}/{COUNT}"),
                Content = new StringContent(content, Encoding.UTF8, MediaTypeNames.Application.Json),
            };

            var response = await _httpClient.SendAsync(request).ConfigureAwait(false);

            if (response.IsSuccessStatusCode)
            {
                var jObject = JObject.Parse(await response.Content.ReadAsStringAsync());
                if (jObject.ContainsKey("count") && jObject["count"] is not null)
                {
                    if (int.TryParse(jObject["count"]?.ToString(), out int result))
                    {
                        return result;
                    }
                    else
                    {
                        return -1;
                    }
                }
                else
                {
                    return -1;
                }
            }
            else
            {
                HandleResponse(response);
                return -1;
            }
        }

        /// <summary>
        /// Filters the plain search results of a search request.
        /// </summary>
        /// <param name="results">Plain JSON result of a search request.</param>
        /// <returns>A dictionary containing the ID of the metadata graphs (key) and the corresponding rankings (value),</returns>
        private static IDictionary<string, JObject> GetSearchResults(JObject results)
        {
            IDictionary<string, JObject> graphNames = new Dictionary<string, JObject>();
            if ((int)results["hits"]["total"]["value"] > 0)
            {
                foreach (var result in results["hits"]["hits"])
                {
                    graphNames.Add((string)result["_source"][ElasticsearchIndexMapper.LABEL_GRAPHNAME], (JObject)result["_source"]);
                }
            }
            return graphNames;
        }
    }
}