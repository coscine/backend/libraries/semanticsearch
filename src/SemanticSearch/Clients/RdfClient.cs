﻿using Coscine.SemanticSearch.Core;
using Coscine.SemanticSearch.Rules;
using Coscine.SemanticSearch.Util;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VDS.RDF;
using VDS.RDF.Parsing;
using VDS.RDF.Query;
using Document = Coscine.SemanticSearch.Util.Document;

namespace Coscine.SemanticSearch.Clients
{
    /// <summary>
    /// Provides all necessary queries to the RDF database to create a mapping of metadata graphs into a document.
    /// </summary>
    public class RdfClient
    {
        public const string LABEL_LITERAL_RULE = "instance";

        private const int QUERY_LIMIT = 1000;
        private const string PLACEHOLDER = "$this";

        private readonly IRdfConnector _connector;
        private readonly DataTypeParser _dataTypeParser;
        public List<string> Languages { get; }

        public readonly string UserUrlPrefix = "https://purl.org/coscine/users";

        private static readonly ILogger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Represents the data types used in the application profiles.
        /// </summary>
        public enum ApplicationProfileType
        {
            CLASS,
            INTEGER,
            DATE,
            STRING,
            BOOLEAN
        }

        public RdfClient(IRdfConnector client, List<string> languageList)
        {
            _connector = client;
            Languages = languageList;
            _dataTypeParser = new DataTypeParser(this);
        }

        /// <summary>
        /// Returns the corresponding application profile of a metadata graph.
        /// </summary>
        /// <param name="graphName">ID of the metadata graph.</param>
        /// <returns>String representation of the application profile URI.</returns>
        public string? GetApplicationProfileOfMetadata(string graphName)
        {
            var _queryString = new SparqlParameterizedString
            {
                CommandText = $@"SELECT ?applicationProfile WHERE {{
               ?resource {Uris.DCTERMS_CONFORMS_TO} ?applicationProfile .
               ?resource {Uris.DCAT_CATALOG}+ ?prov .
               ?prov <{Uris.FdpHasMetadata}> @uri .
            }} LIMIT 1"
            };
            _queryString.SetUri("uri", new Uri(graphName));
            using var results = _connector.QueryWithResultSet(_queryString);
            if (results.Count == 0)
            {
                return null;
            }
            return results.First().Value("applicationProfile").ToString();
        }

        /// <summary>
        /// Returns all application profiles.
        /// </summary>
        /// <returns>String representation of the application profile URI.</returns>
        public IEnumerable<string>? GetApplicationProfiles()
        {
            var _queryString = new SparqlParameterizedString
            {
                CommandText = $@"SELECT DISTINCT ?applicationProfile WHERE {{
                ?applicationProfile a {Uris.SH_NODE_SHAPE}
            }}"
            };
            using var results = _connector.QueryWithResultSet(_queryString);
            if (results.Count == 0)
            {
                return null;
            }
            return results.Select((result) => result.Value("applicationProfile").ToString()).ToList();
        }

        /// <summary>
        /// Marks the metadata graph as deleted.
        /// </summary>
        /// <param name="graphName">ID of the metadata graph.</param>
        public void MarkGraphAsDeleted(string graphName)
        {
            var _queryString = new SparqlParameterizedString
            {
                CommandText = $@"INSERT INTO @uri {{
                @uri {Uris.COSCINE_SEARCH_IS_DELETED} true
            }}"
            };
            _queryString.SetUri("uri", new Uri(graphName));
            _connector.Update(_queryString);
        }

        /// <summary>
        /// Queries the direct classes (without inference and hierarchy) of an instance.
        /// </summary>
        /// <param name="instance">String representation of an instance URI.</param>
        /// <returns></returns>
        public IEnumerable<string> GetDirectClasses(string instance)
        {
            var _queryString = new SparqlParameterizedString
            {
                CommandText = @"SELECT ?class WHERE {{
                @uri a ?class
            }}"
            };
            _queryString.SetUri("uri", new Uri(instance));
            using var results = _connector.QueryWithResultSet(_queryString);

            return results.Select(x => x.Value("class").ToString()).ToList();
        }

        /// <summary>
        /// Queries the parent classes of the given classes.
        /// </summary>
        /// <param name="classes">A list of classes (string representation of the URIs).</param>
        /// <returns>A list of the parent classes.</returns>
        public IList<string> GetParentClasses(IEnumerable<string> classes)
        {
            classes = classes.Select(x => $"<{x}>");
            var filterClasses = string.Join(",", classes);
            using var results = _connector.QueryWithResultSet($@"SELECT * WHERE {{
                ?class {Uris.RDFS_SUBCLASS_OF} ?parent .
                FILTER (?class IN ({filterClasses}))
            }}", false);
            return results.Select(x => x.Value("parent").ToString()).ToList();
        }

        /// <summary>
        /// Queries all labels defined.
        /// </summary>
        /// <returns>An enumerator of the properties (string representation of the URI).</returns>
        public IDictionary<string, List<string>> GetLabels()
        {
            var query = @$" WHERE {{
                {{
                    ?property {Uris.RDFS_LABEL} ?label
                }} UNION {{  
                    ?s {Uris.SH_PATH} ?property .
                    ?s {Uris.SH_NAME} ?label
                }}
            }}";

            using var result = _connector.QueryWithResultSet("SELECT COUNT(*) AS ?count WHERE { SELECT DISTINCT ?property ?label " + query + "}");

            var numberOfResult = Convert.ToInt32(((ILiteralNode)result.First().Value("count")).Value);

            var labels = new Dictionary<string, List<string>>();

            // iterates over results because a query limit exists
            for (var offset = 0; offset < numberOfResult; offset += QUERY_LIMIT)
            {
                using var results = _connector.QueryWithResultSet("SELECT DISTINCT ?property ?label " + query + $" LIMIT {QUERY_LIMIT} OFFSET {offset}");

                foreach (var item in results)
                {
                    var property = "";
                    if (item.Value("property") != null)
                    {
                        property = item.Value("property").ToString();
                    }
                    var label = "";
                    if (item.Value("label") != null)
                    {
                        label = item.Value("label").ToString();
                    }
                    if (!labels.ContainsKey(property))
                    {
                        labels.Add(property, new List<string>());
                    }
                    labels[property].Add(label);
                }
            }

            return labels;
        }

        /// <summary>
        /// Queries all properties (metadata fields) used in the available application profiles.
        /// </summary>
        /// <returns>An enumerator of the properties (string representation of the URI).</returns>
        public IEnumerable<string> GetProperties()
        {
            using var results = _connector.QueryWithResultSet($@"SELECT DISTINCT ?property WHERE {{
                ?profile a {Uris.SH_NODE_SHAPE} .
                ?profile {Uris.SH_PROPERTY} ?profile_property .
                ?profile_property {Uris.SH_PATH} ?property
            }}");
            return results.Select(x => x.Value("property").ToString()).ToList();
        }

        /// <summary>
        /// Queries the IDs of all available metadata graphs in the knowledge graph.
        /// </summary>
        /// <returns>An iterator of an enumerator of the IDs of the metadata graphs.</returns>
        public IEnumerable<IEnumerable<string>> GetAllProjects()
        {
            logger.Info("Receive Projects");
            // Console.WriteLine("Receive Projects");

            var query = $@"{{
                ?g a {Uris.ORG_ORGANIZATIONAL_COLLABORATION} .
                ?g <{Uris.COSCINE_PROJECT_DELETED}> ""false""^^xsd:boolean .
            }}";

            using var result = _connector.QueryWithResultSet("SELECT COUNT(?g) AS ?count " + query);

            var numberOfResult = Convert.ToInt32(((ILiteralNode)result.First().Value("count")).Value);

            var projects = new List<IEnumerable<string>>();

            // iterates over results because a query limit exists
            for (var offset = 0; offset < numberOfResult; offset += QUERY_LIMIT)
            {
                using var results = _connector.QueryWithResultSet("SELECT ?g " + query + $" LIMIT {QUERY_LIMIT} OFFSET {offset}");
                projects.Add(results.Select(x => x.Value("g").ToString()).ToList());
            }

            return projects;
        }

        /// <summary>
        /// Queries the IDs of all available metadata graphs in the knowledge graph.
        /// </summary>
        /// <returns>An iterator of an enumerator of the IDs of the metadata graphs.</returns>
        public IEnumerable<IEnumerable<string>> GetAllResources()
        {
            logger.Info("Receive Resources");
            // Console.WriteLine("Receive Resources");

            var query = $@"{{
                ?p a {Uris.ORG_ORGANIZATIONAL_COLLABORATION} .
                ?p {Uris.DCAT_CATALOG} ?g .
                ?g <{Uris.COSCINE_RESOURCE_DELETED}> ""false""^^xsd:boolean .
            }}";

            using var result = _connector.QueryWithResultSet("SELECT COUNT(?g) AS ?count " + query);

            var numberOfResult = Convert.ToInt32(((ILiteralNode)result.First().Value("count")).Value);

            var resources = new List<IEnumerable<string>>();

            // iterates over results because a query limit exists
            for (var offset = 0; offset < numberOfResult; offset += QUERY_LIMIT)
            {
                using var results = _connector.QueryWithResultSet("SELECT ?g " + query + $" LIMIT {QUERY_LIMIT} OFFSET {offset}");
                resources.Add(results.Select(x => x.Value("g").ToString()).ToList());
            }

            return resources;
        }

        /// <summary>
        /// Queries the IDs of all available metadata graphs in the knowledge graph.
        /// </summary>
        /// <returns>An iterator of an enumerator of the IDs of the metadata graphs.</returns>
        public IEnumerable<IEnumerable<string>> GetAllMetadataIds()
        {
            logger.Info("Receive Metadata Ids");
            // Console.WriteLine("Receive Metadata Ids");

            var query = $@"{{
                ?p a {Uris.ORG_ORGANIZATIONAL_COLLABORATION} .
                ?p <{Uris.COSCINE_PROJECT_DELETED}> ""false""^^xsd:boolean .
                ?p {Uris.DCAT_CATALOG} ?r .
                ?r <{Uris.COSCINE_RESOURCE_DELETED}> ""false""^^xsd:boolean .
                ?r {Uris.DCAT_CATALOG}* ?g .
                ?g a <http://purl.org/fdp/fdp-o#MetadataService> .
                ?g <http://purl.org/fdp/fdp-o#hasMetadata> ?m .
                ?m <http://purl.org/fdp/fdp-o#hasMetadata> ?v .
                OPTIONAL {{ ?v <{Uris.ProvWasInvalidatedBy}> ?invalidatedBy }} . 
                OPTIONAL {{ ?pv <{Uris.ProvWasRevisionOf}>* ?v . ?pv <{Uris.ProvWasInvalidatedBy}> ?invalidatedByParent }} . 
                FILTER (!bound(?invalidatedBy) && !bound(?invalidatedByParent)) .
            }}";

            using var result = _connector.QueryWithResultSet("SELECT COUNT(?v) AS ?count " + query);

            var numberOfResult = Convert.ToInt32(((ILiteralNode)result.First().Value("count")).Value);

            var metadataIds = new List<string>();

            // iterates over results because a query limit exists
            for (var offset = 0; offset < numberOfResult; offset += QUERY_LIMIT)
            {
                logger.Info("Currently retrieving from offset {offset} from number of results {numberOfResult}", offset, numberOfResult);
                // Console.WriteLine($"Currently retrieving from offset {offset} from number of results {numberOfResult}");
                using var results = _connector.QueryWithResultSet("SELECT ?v " + query + $" LIMIT {QUERY_LIMIT} OFFSET {offset}");
                metadataIds.AddRange(results.Select(x => x.Value("v").ToString()).ToList());
            }

            // Remove any version which is not the most recent one
            var removalList = new List<string>();
            foreach (var metadataId in metadataIds)
            {
                var graphName = metadataId[..metadataId.IndexOf("@type")];
                var candidates = metadataIds.Where((otherMetadataId) => otherMetadataId.Contains(graphName)).ToList();
                var mostRecentVersion = candidates.Max()!;
                candidates.Remove(mostRecentVersion);
                removalList.AddRange(candidates);
            }
            removalList = removalList.Distinct().ToList();
            metadataIds.RemoveAll(x => removalList.Contains(x));

            return new List<IEnumerable<string>>
            {
                metadataIds
            };
        }

        /// <summary>
        /// Queries the rdfs:label of an URI.
        /// </summary>
        /// <param name="uri">A string representation of an URI.</param>
        /// <returns></returns>
        public string? GetRdfsLabel(string uri)
        {
            var _queryString = new SparqlParameterizedString
            {
                CommandText = $@"SELECT ?stripped_label WHERE {{
                @uri {Uris.RDFS_LABEL} ?label .
                FILTER (langmatches(lang(?label), ''){BuildLanguageFilterQuery()} ) .
                BIND (STR(?label) AS ?stripped_label)
            }} LIMIT 1"
            };
            _queryString.SetUri("uri", new Uri(uri));
            using var results = _connector.QueryWithResultSet(_queryString);

            if (results.IsEmpty)
            {
                return null;
            }
            else
            {
                return results.First().Value("stripped_label").ToString();
            }
        }

        /// <summary>
        /// Queries the project IDs a user belongs to.
        /// </summary>
        /// <param name="user">A user.</param>
        /// <returns>An enumerator of project IDs.</returns>
        public IEnumerable<string> GetProjectsOfUser(string user)
        {
            if (string.IsNullOrWhiteSpace(user))
            {
                return new List<string>();
            }

            var _queryString = new SparqlParameterizedString
            {
                CommandText = $@"SELECT DISTINCT ?project WHERE {{
                    ?project a {Uris.ORG_ORGANIZATIONAL_COLLABORATION} .
                    ?project <http://www.w3.org/2006/vcard/ns#hasMember> @user
                }}"
            };
            _queryString.SetUri("user", new Uri("https://purl.org/coscine/users/" + user));
            using var results = _connector.QueryWithResultSet(_queryString);

            return results.Select(x =>
            {
                IUriNode nameNode = (IUriNode)x.Value("project");
                var projectUri = nameNode.Uri.ToString();
                // TODO: Fix project Uri
                var projectId = projectUri[(projectUri.LastIndexOf("/") + 1)..];
                return $"https://purl.org/coscine/projects/{projectId}";
            }).ToList();
        }

        /// <summary>
        /// Queries the sh:names of a property in all available application profiles.
        /// </summary>
        /// <param name="property">A string representation of a property URI.</param>
        /// <returns>An enumerator of names.</returns>
        public IEnumerable<string> GetApplicationProfilesNamesOfProperty(string property)
        {
            var _queryString = new SparqlParameterizedString
            {
                CommandText = $@"SELECT DISTINCT ?name WHERE {{
                    ?profile_property {Uris.SH_PATH} @uri .
                    ?profile_property {Uris.SH_NAME} ?name
                }}"
            };
            _queryString.SetUri("uri", new Uri(property));
            using var results = _connector.QueryWithResultSet(_queryString);

            return results.Select(x =>
            {
                ILiteralNode nameNode = (ILiteralNode)x.Value("name");
                return nameNode.Value;
            }).ToList();
        }

        /// <summary>
        /// Queries the application profile data type of all properties.
        /// </summary>
        /// <remarks>Properties that have different data types (due to different profiles) are first
        /// defined as a class that maps to text.</remarks>
        /// <returns></returns>
        public IDictionary<string, List<ApplicationProfileType>> GetPropertyTypes()
        {
            logger.Info("Get Property Types");
            // Console.WriteLine($"Get Property Types");
            var _queryString = new SparqlParameterizedString
            {
                CommandText = $@"PREFIX sh: <http://www.w3.org/ns/shacl#>

            SELECT DISTINCT ?type ?class ?path WHERE {{
                ?profile {Uris.SH_PROPERTY} ?profile_property .
                ?profile_property {Uris.SH_PATH} ?path .
                ?profile a {Uris.SH_NODE_SHAPE} .
                {{
                    ?profile_property {Uris.SH_CLASS} ?class .
                }}
                UNION
                {{
                    ?profile_property {Uris.SH_DATATYPE} ?type .
                }}
            }}"
            };
            using var results = _connector.QueryWithResultSet(_queryString);

            var propertyTypes = new Dictionary<string, List<ApplicationProfileType>>();

            foreach (var entry in results)
            {
                var classes = entry.Value("class")?.ToString();
                var type = entry.Value("type")?.ToString();
                var path = entry.Value("path")?.ToString();

                var profileType = ApplicationProfileType.CLASS;

                if (string.IsNullOrEmpty(classes) && !string.IsNullOrEmpty(type))
                {
                    // unique data type
                    profileType = GetDataType(type);
                }
                if (!string.IsNullOrEmpty(path))
                {
                    if (!propertyTypes.ContainsKey(path))
                    {
                        propertyTypes.Add(path, new List<ApplicationProfileType>());
                    }
                    propertyTypes[path].Add(profileType);
                }
            }

            return propertyTypes;
        }

        /// <summary>
        /// Queries the application profile data type of a property.
        /// </summary>
        /// <remarks>Properties that have different data types (due to different profiles) are first
        /// defined as a class that maps to text.</remarks>
        /// <param name="property">String representation of a properties URI.</param>
        /// <returns></returns>
        public ApplicationProfileType GetTypeOfProperty(string property)
        {
            logger.Info("Get Type of property {property}", property);
            // Console.WriteLine($"Get Type of property {property}");
            var _queryString = new SparqlParameterizedString
            {
                CommandText = $@"SELECT DISTINCT ?type ?class WHERE {{
                ?profile {Uris.SH_PROPERTY} ?profile_property .
                ?profile_property {Uris.SH_PATH} @uri .
                ?profile a {Uris.SH_NODE_SHAPE} .
                {{
                    ?profile_property {Uris.SH_CLASS} ?class .
                }}
                UNION
                {{
                    ?profile_property {Uris.SH_DATATYPE} ?type .
                }}
            }}"
            };
            _queryString.SetUri("uri", new Uri(property));
            using var results = _connector.QueryWithResultSet(_queryString);

            var classes = results.Where(x => x.Value("class") != null).Select(x => x.Value("class").ToString()).ToList();
            var datatypes = results.Where(x => x.Value("type") != null).Select(x => x.Value("type").ToString()).ToList();

            if (classes.Count == 0 && datatypes.Count == 1)
            {
                // unique data type
                var type = datatypes[0];
                return GetDataType(type);
            }
            else
            {
                // class or ambiguous data type
                // classes will be mapped to text
                return ApplicationProfileType.CLASS;
            }
        }

        /// <summary>
        /// Maps the XSD data types to the corresponding application profile type.
        /// </summary>
        /// <param name="type">XSD data type.</param>
        /// <returns>An application profile type.</returns>
        public ApplicationProfileType GetDataType(string type)
        {
            return type switch
            {
                XmlSpecsHelper.XmlSchemaDataTypeBoolean => ApplicationProfileType.BOOLEAN,
                XmlSpecsHelper.XmlSchemaDataTypeInteger => ApplicationProfileType.INTEGER,
                XmlSpecsHelper.XmlSchemaDataTypeDateTime => ApplicationProfileType.DATE,
                XmlSpecsHelper.XmlSchemaDataTypeDate => ApplicationProfileType.DATE,
                _ => ApplicationProfileType.STRING,
            };
        }

        /// <summary>
        /// Queries the current index version.
        /// </summary>
        /// <returns>Number of the current index version.</returns>
        public int GetCurrentIndexVersion()
        {
            using var result = _connector.QueryWithResultSet($@"SELECT ?version FROM <{Uris.COSCINE_SEARCH_CURRENT_INDEX}> WHERE {{
                <{Uris.COSCINE_SEARCH_CURRENT_INDEX}> <{Uris.COSCINE_SEARCH_HAS_INDEX_VERSION}> ?version
            }}", false);
            return result.IsEmpty ? 1 : Convert.ToInt32(((ILiteralNode)result.First().Value("version")).Value);
        }

        /// <summary>
        /// Updates the current index version.
        /// </summary>
        /// <param name="newVersion">New index version.</param>
        public void SetCurrentIndexVersion(int newVersion)
        {
            var _queryString = new SparqlParameterizedString
            {
                CommandText = $@"DELETE WHERE {{
                GRAPH <{Uris.COSCINE_SEARCH_CURRENT_INDEX}> {{
                    <{Uris.COSCINE_SEARCH_CURRENT_INDEX}> <{Uris.COSCINE_SEARCH_HAS_INDEX_VERSION}> ?version
                }}
            }};
            INSERT DATA {{
                GRAPH <{Uris.COSCINE_SEARCH_CURRENT_INDEX}> {{
                    <{Uris.COSCINE_SEARCH_CURRENT_INDEX}> <{Uris.COSCINE_SEARCH_HAS_INDEX_VERSION}> @literal
                }}
            }}"
            };
            _queryString.SetLiteral("literal", newVersion);
            _connector.Update(_queryString);
        }

        /// <summary>
        /// Queries all literal rules of a specific named graph.
        /// </summary>
        /// <param name="graphName">Name of the graph.</param>
        /// <returns>A dictionary containing the classes (key) and corresponding literal rules (value) of the graph.</returns>
        public Dictionary<string, LiteralRule> ConstructLiteralRules(string graphName)
        {
            var _queryString = new SparqlParameterizedString
            {
                CommandText = $@"SELECT ?class ?construct ?prefixName FROM @uri WHERE {{
                ?class {Uris.SH_RULE} ?rule .
                ?rule {Uris.SH_CONSTRUCT} ?construct .
                ?rule {Uris.SH_PREFIXES} ?prefixName .
                ?rule a {Uris.SH_SPARQL_RULE} .
                OPTIONAL {{
                    ?rule {Uris.SH_ORDER} ?order
                }}
            }} ORDER BY ?order"
            };
            _queryString.SetUri("uri", new Uri(graphName));
            using var results = _connector.QueryWithResultSet(_queryString);

            return results.ToDictionary(
                x => x.Value("class").ToString(),
                x =>
                {
                    SparqlParameterizedString queryString = new()
                    {
                        CommandText = x.Value("construct").ToString().Replace(PLACEHOLDER, $"@{LABEL_LITERAL_RULE}")
                    };

                    var prefixName = x.Value("prefixName").ToString();

                    foreach (var prefixResult in GetPrefixesOfGraph(graphName, prefixName))
                    {
                        queryString.Namespaces.AddNamespace(prefixResult.Key, new Uri(prefixResult.Value));
                    }
                    return new LiteralRule(_connector, queryString.CommandText, queryString.Namespaces, Languages);
                }
            );
        }

        /// <summary>
        /// Queries all prefix definitions in a specific graph and of a specific prefix rule.
        /// </summary>
        /// <param name="graphName">Name of the graph.</param>
        /// <param name="prefixName">Name of the prefix rule.</param>
        /// <returns>A dictionary containing the prefixes (keys) and namespaces (value) of a prefix rule.</returns>
        private IDictionary<string, string> GetPrefixesOfGraph(string graphName, string prefixName)
        {
            var _queryString = new SparqlParameterizedString
            {
                CommandText = $@"SELECT ?prefix STR(?namespace) AS ?namespaceLabel FROM @graph WHERE {{
                @prefix {Uris.SH_DECLARE} ?prefixRule .
                ?prefixRule {Uris.SH_PREFIX} ?prefix .
                ?prefixRule {Uris.SH_NAMESPACE} ?namespace .
                @prefix a {Uris.OWL_ONTOLOGY} .
            }}"
            };
            _queryString.SetUri("graph", new Uri(graphName));
            _queryString.SetUri("prefix", new Uri(prefixName));
            using var results = _connector.QueryWithResultSet(_queryString);
            return results.ToDictionary(x => x.Value("prefix").ToString(), x => x.Value("namespaceLabel").ToString());
        }

        /// <summary>
        /// Help function to guess label based on the URI.
        /// </summary>
        /// <param name="element">String representation of an URI.</param>
        /// <returns>Guessed label.</returns>
        public string? GuessLabel(string element)
        {
            logger.Info("Guess label for {element}", element);
            // Console.WriteLine($"Guess label for {element}");
            if (element.Contains('#'))
            {
                return element.Split('#').Last();
            }
            else if (element.Contains('/'))
            {
                var splitted = element.Split('/');
                var last = splitted.Last();
                if (!string.IsNullOrEmpty(last))
                {
                    return last;
                }
                else
                {
                    return splitted[^2];
                }
            }
            else if (element.StartsWith("_:"))
            {
                return "BlankNode";
            }
            else
            {
                logger.Warn("No label could be guessed for {element}", element);
                // Console.WriteLine($"No label could be guessed for {element}");
                return null;
            }
        }

        /// <summary>
        /// Returns triples of a graph.
        /// </summary>
        /// <param name="graphName">ID of the graph.</param>
        /// <returns>An enumerator of triples.</returns>
        public IGraph GetGraph(string graphName)
        {
            return _connector.GetGraph(graphName);
        }

        /// <summary>
        /// Creates key value pairs from the given triples.
        /// </summary>
        /// <param name="triples">An enumerator of triples which needs to be parsed.</param>
        /// <param name="profile">The application profile which the triples belong to.</param>
        /// <param name="indexMapper">The <c>ElasticsearchIndexMapper</c>.</param>
        /// <returns></returns>
        public Document CreateFields(IEnumerable<Triple> triples, SpecificApplicationProfile? profile, ElasticsearchIndexMapper indexMapper)
        {
            var document = new Document();
            foreach (var triple in triples)
            {
                var property = triple.Predicate.ToString();
                if (string.Equals(property, Uris.RDF_TYPE_LONG))
                {
                    continue;
                }

                var label = indexMapper.GetLabelOfProperty(property);
                if (string.IsNullOrEmpty(label))
                {
                    logger.Warn("Property {property} could not be indexed because no label was found.", property);
                    // Console.WriteLine($"Property {property} could not be indexed because no label was found.");
                    continue;
                }
                try
                {
                    var toAddDocument = _dataTypeParser.Parse(label, triple.Object, indexMapper, profile);
                    if (toAddDocument != null)
                    {
                        document.Merge(toAddDocument);
                    }
                }
                catch (NotIndexableException e)
                {
                    logger.Warn("Property {property} could not be indexed. Reason: {reason}", property, e.Message);
                    // Console.WriteLine($"Property {property} could not be indexed. Reason: {e.Reason}");
                    continue;
                }
                catch (FormatException e)
                {
                    logger.Warn("Property {property} could not be indexed. Reason: {message}", property, e.Message);
                    // Console.WriteLine($"Property {property} could not be indexed. Reason: {e.Message}");
                    continue;
                }
            }
            return document;
        }

        private string BuildLanguageFilterQuery()
        {
            var stringBuilder = new StringBuilder();
            foreach (var lang in Languages)
            {
                /* Pattern:
                 * || langmatches(lang(?label), '{lang}')
                 */
                stringBuilder.Append(" || langmatches(lang(?label), '").Append(lang).Append("')");
            }
            return stringBuilder.ToString();
        }
    }
}