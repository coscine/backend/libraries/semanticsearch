﻿using Coscine.SemanticSearch.Util;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using VDS.RDF;

namespace Coscine.SemanticSearch.TripleCreator
{
    public class MetadataTripleCreator : DefaultTripleCreator
    {
        private static readonly Regex guidRegexPattern = new("([0-9A-Fa-f]{8}[-][0-9A-Fa-f]{4}[-][0-9A-Fa-f]{4}[-][0-9A-Fa-f]{4}[-][0-9A-Fa-f]{12})");
        private readonly IEnumerable<string> _publicResources;
        private readonly IDictionary<string, string> _resourceToProject;
        private readonly IDictionary<string, string> _resourceToApplicationProfile;
        private readonly IDictionary<string, string> _projectSlugs;

        private const string _resourcesPrefix = "https://purl.org/coscine/resources/";

        private static readonly ILogger logger = LogManager.GetCurrentClassLogger();

        public MetadataTripleCreator(IEnumerable<string> publicResources, IDictionary<string, string> resourceToProject, IDictionary<string, string> resourceToApplicationProfile, IDictionary<string, string> projectSlugs)
        {
            _publicResources = publicResources;
            _resourceToProject = resourceToProject;
            _resourceToApplicationProfile = resourceToApplicationProfile;
            _projectSlugs = projectSlugs;
        }

        public override Document CreateFields(IGraph graph, ElasticsearchIndexMapper indexMapper)
        {
            var coscineFields = new List<string>()
            {
                Uris.COSCINE_SEARCH_BELONGS_TO_PROJECT,
                Uris.COSCINE_SEARCH_ABSOLUTEFILENAME,
                Uris.COSCINE_SEARCH_APPLICATION_PROFILE,
                Uris.COSCINE_SEARCH_FILENAME,
                Uris.COSCINE_SEARCH_GRAPHNAME,
                Uris.COSCINE_SEARCH_HOMEPAGE,
                Uris.COSCINE_SEARCH_VERSION,
                Uris.COSCINE_SEARCH_STRUCTURE_TYPE,
                Uris.COSCINE_PROJECTSTRUCTURE_IS_PUBLIC_LONG
            };
            var result = new Document();

            var resourceId = guidRegexPattern.Match(graph.BaseUri.ToString()).Value; // Handle null.Value case?

            foreach (var property in coscineFields)
            {
                var label = indexMapper.GetLabelOfProperty(property);
                if (string.IsNullOrEmpty(label))
                {
                    logger.Info("Property {property} could not be indexed because no label was found.", property);
                    // Console.WriteLine($"Property {property} could not be indexed because no label was found.");
                    continue;
                }
                switch (property)
                {
                    case Uris.COSCINE_SEARCH_BELONGS_TO_PROJECT:
                        if (_resourceToProject.ContainsKey(resourceId))
                        {
                            // Expected correct URI format for Projects: https://purl.org/coscine/projects/{ProjectGUID}
                            result.Add(label, $"https://purl.org/coscine/projects/{_resourceToProject[resourceId]}");
                        }
                        else
                        {
                            result.Add(label, "");
                        }
                        break;

                    case Uris.COSCINE_SEARCH_ABSOLUTEFILENAME:
                        result.Add(label, ReceivePath(graph, true));
                        break;

                    case Uris.COSCINE_SEARCH_APPLICATION_PROFILE:
                        if (_resourceToApplicationProfile.ContainsKey(resourceId))
                        {
                            result.Add(label, _resourceToApplicationProfile[resourceId]);
                        }
                        else
                        {
                            result.Add(label, "");
                        }
                        break;

                    case Uris.COSCINE_SEARCH_FILENAME:
                        result.Add(label, ReceivePath(graph, false));
                        break;

                    case Uris.COSCINE_SEARCH_GRAPHNAME:
                        result.Add(label, ReceiveGraphName(graph));
                        break;

                    case Uris.COSCINE_SEARCH_HOMEPAGE:
                        if (_resourceToProject.ContainsKey(resourceId) && _projectSlugs.ContainsKey(_resourceToProject[resourceId]))
                        {
                            result.Add(label, $"https://coscine.rwth-aachen.de/p/{_projectSlugs[_resourceToProject[resourceId]]}/r/{resourceId}/#/{ReceivePath(graph, true)}");
                        }
                        else
                        {
                            result.Add(label, ReceiveGraphName(graph));
                        }
                        break;

                    case Uris.COSCINE_SEARCH_VERSION:
                        if (graph.BaseUri.ToString().Contains("version="))
                        {
                            var version = graph.BaseUri.ToString()[(graph.BaseUri.ToString().IndexOf("version=") + "version=".Length)..];
                            result.Add(label, version);
                        }
                        break;

                    case Uris.COSCINE_SEARCH_STRUCTURE_TYPE:
                        result.Add(label, "https://purl.org/coscine/terms/structure#Metadata");
                        break;

                    case Uris.COSCINE_PROJECTSTRUCTURE_IS_PUBLIC_LONG:
                        result.Add(label, _publicResources.Contains(resourceId));
                        break;

                    default:
                        continue;
                }
            }
            return result;
        }

        private static string ReceiveGraphName(IGraph graph)
        {
            var graphName = graph.BaseUri.ToString();
            if (graphName.Contains("@path") || !graphName.Contains("@type"))
            {
                return graphName;
            }

            return graphName[..graphName.IndexOf("@type")];
        }

        private static string ReceivePath(IGraph graph, bool absolute = false)
        {
            var graphName = graph.BaseUri.ToString();
            var path = graphName;
            if (path.Contains("@path"))
            {
                path = path[(path.IndexOf("@path=") + "@path=".Length)..];
                path = Uri.UnescapeDataString(path);
                if (path.StartsWith("/"))
                {
                    path = path[1..];
                }
            }
            else if (path.Contains(_resourcesPrefix))
            {
                path = path.Replace(_resourcesPrefix, "");
                // Remove Resource Id
                path = path[(path.IndexOf('/') + 1)..];
                if (path.Contains("@type"))
                {
                    path = path[..path.IndexOf("@type")];
                }
                if (path.EndsWith('/'))
                {
                    path = path[..^1];
                }
            }

            if (!absolute)
            {
                path = path.Split('/').Last();
            }

            return path;
        }
    }
}