﻿using Coscine.SemanticSearch.Util;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using VDS.RDF;

namespace Coscine.SemanticSearch.TripleCreator
{
    public class ResourceTripleCreator : DefaultTripleCreator
    {
        private static readonly Regex guidRegexPattern = new("([0-9A-Fa-f]{8}[-][0-9A-Fa-f]{4}[-][0-9A-Fa-f]{4}[-][0-9A-Fa-f]{4}[-][0-9A-Fa-f]{12})");
        private readonly ICollection<string> _publicResources;
        private readonly IDictionary<string, string> _resourceToProject;
        private readonly IDictionary<string, string> _resourceToApplicationProfile;

        private static readonly ILogger logger = LogManager.GetCurrentClassLogger();

        public ResourceTripleCreator(ICollection<string> publicResources, IDictionary<string, string> resourceToProject, IDictionary<string, string> resourceToApplicationProfile)
        {
            _publicResources = publicResources;
            _resourceToProject = resourceToProject;
            _resourceToApplicationProfile = resourceToApplicationProfile;
        }

        public override Document CreateFields(IGraph graph, ElasticsearchIndexMapper indexMapper)
        {
            var coscineFields = new List<string>()
            {
                Uris.COSCINE_SEARCH_APPLICATION_PROFILE,
                Uris.COSCINE_SEARCH_BELONGS_TO_PROJECT,
                Uris.COSCINE_SEARCH_GRAPHNAME,
                Uris.COSCINE_SEARCH_STRUCTURE_TYPE,
                Uris.COSCINE_PROJECTSTRUCTURE_IS_PUBLIC_LONG
            };
            var result = new Document();

            var resourceId = guidRegexPattern.Match(graph.BaseUri.ToString()).Value; // Handle null.Value case?

            foreach (var property in coscineFields)
            {
                var label = indexMapper.GetLabelOfProperty(property);
                if (string.IsNullOrEmpty(label))
                {
                    logger.Info("Property {property} could not be indexed because no label was found.", property);
                    // Console.WriteLine($"Property {property} could not be indexed because no label was found.");
                    continue;
                }
                switch (property)
                {
                    case Uris.COSCINE_SEARCH_APPLICATION_PROFILE:
                        foreach (var triple in graph.GetTriplesWithPredicate(new Uri("http://purl.org/dc/terms/conformsTo")))
                        {
                            var applicationProfile = triple.Object.ToString();
                            result.Add(label, applicationProfile);
                            _resourceToApplicationProfile.Add(resourceId, applicationProfile);
                            break;
                        }
                        break;

                    case Uris.COSCINE_SEARCH_BELONGS_TO_PROJECT:
                        foreach (var triple in graph.GetTriplesWithPredicate(new Uri("http://www.w3.org/ns/auth/acl#agentGroup")))
                        {
                            var projectId = triple.Object.ToString();
                            projectId = projectId[(projectId.LastIndexOf("/") + 1)..];
                            result.Add(label, $"https://purl.org/coscine/projects/{projectId}");
                            _resourceToProject.Add(resourceId, projectId);
                            break;
                        }
                        break;

                    case Uris.COSCINE_SEARCH_GRAPHNAME:
                        result.Add(label, graph.BaseUri.ToString());
                        break;

                    case Uris.COSCINE_SEARCH_STRUCTURE_TYPE:
                        result.Add(label, "https://purl.org/coscine/terms/structure#Resource");
                        break;

                    case Uris.COSCINE_PROJECTSTRUCTURE_IS_PUBLIC_LONG:
                        var isPublic = graph.GetTriplesWithPredicate(new Uri("https://purl.org/coscine/terms/resource#visibility"))
                            .Any((triple) => triple.Object.ToString() == "https://purl.org/coscine/terms/visibility#public");
                        result.Add(label, isPublic);
                        if (isPublic)
                        {
                            _publicResources.Add(resourceId);
                        }
                        break;

                    default:
                        continue;
                }
            }
            return result;
        }
    }
}