﻿using CommandLine;
using Coscine.SemanticSearch.Clients;
using Coscine.SemanticSearch.Core;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Coscine.SemanticSearch.Cmd
{
    /// <summary>
    /// Entry class which interprets the command line arguments and calls corresponding functions.
    /// </summary>
    public class Program
    {
        // provided methods
        private const string COUNT = "count";

        private const string SEARCH = "search";
        private const string REINDEX = "reindex";
        private const string INDEX = "index";
        private const string DELETE = "delete";
        private const string ADD = "add";

        // language list
        private static readonly List<string> languageList = new List<string>()
        {
            "en",
            "de"
        };

        /// <summary>
        /// Class to define valid options, and to receive the parsed options.
        /// </summary>
        public class Options
        {
            // required parameter
            [Option('a', "action", Required = true, HelpText = "Possible action: search, count, reindex, index, delete or add")]
            public string Action { get; set; }

            // optional parameter
            [Option('m', Default = "http://localhost:8890/sparql", HelpText = "The link to the SPARQL connection")]
            public string MetadataStore { get; set; }

            [Option('e', Default = "localhost", HelpText = "Server name of Elasticsearch")]
            public string ElasticsearchServer { get; set; }

            [Option("ep", Default = 9200, HelpText = "Port of Elasticsearch")]
            public int ElasticsearchPort { get; set; }

            // dependent on action

            // add/update/delete
            [Option('d', "doc", HelpText = "ID of metadata graph")]
            public string Document { get; set; }

            // search and count
            [Option('q', "query", Default = "*", HelpText = "Elasticsearch query")]
            public string Query { get; set; }

            [Option("adv", Default = false, HelpText = "Set true for advanced Elasticsearch search syntax")]
            public bool Advanced { get; set; }

            [Option('u', "user", Default = "", HelpText = "Specify user or only public metadata records could be found")]
            public string User { get; set; }
        }

        public static void Main(string[] args)
        {
            _ = Parser.Default.ParseArguments<Options>(args)
                .WithParsed<Options>(o => RunSemanticSearch(o).Wait());
        }

        private static async Task RunSemanticSearch(Options o)
        {
            IRdfConnector connector = new VirtuosoRdfConnector(o.MetadataStore);
            ISearchClient searchClient = new ElasticsearchSearchClient(o.ElasticsearchServer, "" + o.ElasticsearchPort);
            var mapper = new RdfSearchMapper(connector, searchClient, languageList);

            // execute method specified in command line arguments
            switch (o.Action)
            {
                case COUNT:
                    {
                        var result = await mapper.CountAsync(o.Query, o.User, o.Advanced);
                        Console.WriteLine(result);
                        break;
                    }

                case SEARCH:
                    {
                        var results = await mapper.SearchAsync(o.Query, o.User, o.Advanced);
                        foreach (var result in results)
                        {
                            Console.WriteLine("[{0};    {1}]", result.Value, result.Key);
                        }
                        break;
                    }

                case REINDEX:
                    await mapper.ReIndexAsync();
                    break;

                case INDEX:
                    await mapper.CreateIndexAsync();
                    break;

                case ADD:
                    if (CheckDocumentIsSet(o.Document))
                    {
                        await mapper.AddDocumentAsync(o.Document);
                    }
                    break;

                case DELETE:
                    if (CheckDocumentIsSet(o.Document))
                    {
                        await mapper.DeleteDocumentAsync(o.Document);
                    }
                    break;

                default:
                    Console.WriteLine("Action parameter does not exist");
                    break;
            }
        }

        /// <summary>
        /// Checks if a document is set.
        /// </summary>
        /// <param name="doc">ID of the metadata graph.</param>
        /// <returns></returns>
        private static bool CheckDocumentIsSet(string doc)
        {
            if (string.IsNullOrEmpty(doc))
            {
                Console.WriteLine("A document needs to be specified");
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}