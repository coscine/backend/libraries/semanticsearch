var searchData=
[
  ['rdf_168',['RDF',['../class_semantic_search_implementation_1_1_uris.html#a212997e1ebc87e8b0c0c5e4ee21055d2',1,'SemanticSearchImplementation::Uris']]],
  ['rdf_5ftype_5flong_169',['RDF_TYPE_LONG',['../class_semantic_search_implementation_1_1_uris.html#a35f8b9626785a8781fd105a200d00240',1,'SemanticSearchImplementation::Uris']]],
  ['rdfclient_170',['RdfClient',['../class_semantic_search_implementation_1_1_rdf_client.html',1,'SemanticSearchImplementation.RdfClient'],['../class_semantic_search_implementation_1_1_rdf_client.html#a425c1a2d8643747f852c1cce9ee0d88e',1,'SemanticSearchImplementation.RdfClient.RdfClient()']]],
  ['rdfclient_2ecs_171',['RdfClient.cs',['../_rdf_client_8cs.html',1,'']]],
  ['rdfs_172',['RDFS',['../class_semantic_search_implementation_1_1_uris.html#af78d25ac43f5f2b9e43bd544f9099470',1,'SemanticSearchImplementation::Uris']]],
  ['rdfs_5flabel_173',['RDFS_LABEL',['../class_semantic_search_implementation_1_1_uris.html#a9554faa858bdbdfeeac34a042daff151',1,'SemanticSearchImplementation::Uris']]],
  ['rdfs_5fsubclass_5fof_174',['RDFS_SUBCLASS_OF',['../class_semantic_search_implementation_1_1_uris.html#ad19dd21c8b3935b0cf41595c34c9b031',1,'SemanticSearchImplementation::Uris']]],
  ['rdfsearchmapper_175',['RdfSearchMapper',['../class_semantic_search_implementation_1_1_rdf_search_mapper.html',1,'SemanticSearchImplementation.RdfSearchMapper'],['../class_semantic_search_implementation_1_1_rdf_search_mapper.html#a881f6f1e97e793fdfedaef0c714548f7',1,'SemanticSearchImplementation.RdfSearchMapper.RdfSearchMapper()']]],
  ['rdfsearchmapper_2ecs_176',['RdfSearchMapper.cs',['../_rdf_search_mapper_8cs.html',1,'']]],
  ['readme_2emd_177',['readme.md',['../readme_8md.html',1,'']]],
  ['reason_178',['Reason',['../class_semantic_search_implementation_1_1_not_indexable_exception.html#a52ef9eeaf159ce899e1519eb3f3f482d',1,'SemanticSearchImplementation::NotIndexableException']]],
  ['reindex_179',['REINDEX',['../class_semantic_search_implementation_1_1_semantic_search.html#a3df2e52026edac177f0f2155af4b71c5',1,'SemanticSearchImplementation::SemanticSearch']]],
  ['reindexasync_180',['ReIndexAsync',['../class_semantic_search_implementation_1_1_rdf_search_mapper.html#a9cd26a7bc78359fdd781ad108c828af9',1,'SemanticSearchImplementation::RdfSearchMapper']]],
  ['replacemapping_181',['ReplaceMapping',['../class_semantic_search_implementation_1_1_elasticsearch_index_mapper.html#abec997d6b8e4caa78a5089676e8e0ffa',1,'SemanticSearchImplementation::ElasticsearchIndexMapper']]],
  ['ruleset_5fquery_182',['RULESET_QUERY',['../class_semantic_search_implementation_1_1_virtuoso_rdf_connector.html#a3fa556b03971c64b7aa44586a89bf14f',1,'SemanticSearchImplementation::VirtuosoRdfConnector']]]
];
