var searchData=
[
  ['org_405',['ORG',['../class_semantic_search_implementation_1_1_uris.html#aed0bf07b959bf761bcbdccda2d652c14',1,'SemanticSearchImplementation::Uris']]],
  ['org_5fhas_5funit_406',['ORG_HAS_UNIT',['../class_semantic_search_implementation_1_1_uris.html#ad4fafe0582c322eea24e77fe735b395f',1,'SemanticSearchImplementation::Uris']]],
  ['org_5fmember_407',['ORG_MEMBER',['../class_semantic_search_implementation_1_1_uris.html#a6548c190196cd940ed3a05dee17e023e',1,'SemanticSearchImplementation::Uris']]],
  ['org_5fmembership_408',['ORG_MEMBERSHIP',['../class_semantic_search_implementation_1_1_uris.html#a4f92f611584a589e3b4626f98e26a5bc',1,'SemanticSearchImplementation::Uris']]],
  ['org_5forganization_409',['ORG_ORGANIZATION',['../class_semantic_search_implementation_1_1_uris.html#a21d3d824fdd4a2db015e074eb319b0c0',1,'SemanticSearchImplementation::Uris']]],
  ['owl_410',['OWL',['../class_semantic_search_implementation_1_1_uris.html#ad5c26bfebd231bef2523f4b49f0bfeb9',1,'SemanticSearchImplementation::Uris']]],
  ['owl_5fontology_411',['OWL_ONTOLOGY',['../class_semantic_search_implementation_1_1_uris.html#a7f57526329e4f4a74972b7eb6381153d',1,'SemanticSearchImplementation::Uris']]]
];
